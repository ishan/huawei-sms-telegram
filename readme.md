
# Huawei SMS Telegram


This is a telegram bot to see messages sent to your number used in a Huawei 5G Modem. The Huawei API is more or less the same across various product lines so it might work with other huawei products as well


# How to use ?

## Build from source

```
git clone https://git.ishanjain.me/ishan/huawei-sms-telegram.git
cd huawei-sms-telegram
cargo b --release
```

## Grab the binary from the releases section


## Environment Variables

```
export TELOXIDE_TOKEN=<telegram-bot-token>
export RUST_LOG=info
export HUAWEI_PASSWORD=<huawei-modem-password. username is admin>
```




