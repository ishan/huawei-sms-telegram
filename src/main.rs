#![feature(future_join)]
mod huawei;

use crate::huawei::Huawei;
use log::info;
use std::net::{Ipv4Addr, SocketAddrV4};
use teloxide::{
    payloads::SendMessageSetters, prelude::*, repls::CommandReplExt, requests::ResponseResult,
    types::Message, utils::command::BotCommands, Bot,
};

const VALID_CHAT_GROUPS: &[i64] = &[-1002048150741];

#[tokio::main]
async fn main() {
    env_logger::init();
    info!("Starting bot...");

    let bot = Bot::from_env();

    Command::repl(bot, answer).await;
}

#[derive(BotCommands, Clone)]
#[command(rename_rule = "lowercase", description = "Supported Commands")]
enum Command {
    #[command(description = "Fetch Messages")]
    SMS,
}

async fn answer(bot: Bot, msg: Message, cmd: Command) -> ResponseResult<()> {
    let thread_id = if let Some(thread_id) = msg.thread_id {
        thread_id
    } else {
        return Ok(());
    };

    // Disallow any one but ishan's homelab group
    if !VALID_CHAT_GROUPS.iter().any(|&x| x == msg.chat.id.0) {
        return Ok(());
    }

    match cmd {
        Command::SMS => {
            let password = std::env::var("HUAWEI_PASSWORD").unwrap();
            let mut huawei = Huawei::new(
                SocketAddrV4::new(Ipv4Addr::new(192, 168, 8, 1), 443),
                "admin",
                &password,
            );

            if let Err(e) = huawei.login().await {
                bot.send_message(msg.chat.id, format!("error in logging in: {:?}", e))
                    .message_thread_id(thread_id)
                    .await?;

                return Ok(());
            }

            let sms = match huawei.list_sms(3).await {
                Ok(v) => v,
                Err(e) => {
                    bot.send_message(msg.chat.id, format!("an error occured {}", e))
                        .message_thread_id(thread_id)
                        .await?;
                    return Ok(());
                }
            };

            for sms in sms {
                bot.send_message(
                    msg.chat.id,
                    format!(
                        "From: {}\nBody: {}\nTime: {}",
                        sms.phone, sms.content, sms.date
                    ),
                )
                .message_thread_id(thread_id)
                .await?;
            }

            Ok(())
        }
    }
}
