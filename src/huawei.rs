use hmac::{Hmac, Mac};
use log::info;
use pbkdf2::pbkdf2_hmac;
use rand::RngCore;
use reqwest::{Body, Client, Error, Response};
use serde::{Deserialize, Serialize};
use serde_xml::value::Element;
use sha2::{Digest, Sha256};
use std::net::SocketAddr;
use std::time::Instant;

pub struct Huawei {
    username: String,
    password: String,
    address: SocketAddr,
    http_client: Client,
    request_verification_token: String,
    client_nonce: String,
    server_token: Option<String>,
    pub logged_in_at: Option<Instant>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct HuaweiError {
    #[serde(rename = "code")]
    code: String,

    #[serde(rename = "message")]
    message: String,
}

#[derive(Serialize, Debug)]
#[serde(rename = "request")]
struct ChallengeLoginRequest {
    username: String,
    firstnonce: String,
    mode: String,
}

#[derive(Clone, Debug, Deserialize)]
struct ChallengeLoginResponse {
    salt: String,
    modeselected: String,
    servernonce: String,
    #[serde(rename = "newType")]
    new_type: String,
    iterations: u32,
}

#[derive(Serialize, Debug)]
#[serde(rename = "request")]
struct AuthenticationLoginRequest {
    clientproof: String,
    finalnonce: String,
}

impl Huawei {
    pub fn new(addr: impl Into<SocketAddr>, username: &str, password: &str) -> Self {
        Self {
            username: username.to_string(),
            password: password.to_string(),
            address: addr.into(),
            http_client: Client::builder()
                .danger_accept_invalid_certs(true)
                .http1_title_case_headers()
                .cookie_store(true)
                .build()
                .expect("error in building http client"),
            request_verification_token: String::new(),
            client_nonce: String::new(),
            server_token: None,
            logged_in_at: None,
        }
    }

    pub async fn login(&mut self) -> Result<(), HuaweiError> {
        let challenge_resp = self.api_challenge().await?;

        let client_proof = self
            .generate_client_proof(challenge_resp.clone())
            .map_err(|e| HuaweiError {
                code: "generate_client_proof".to_string(),
                message: e.to_string(),
            })?;

        let input = AuthenticationLoginRequest {
            clientproof: client_proof,
            finalnonce: challenge_resp.servernonce,
        };
        let body = serde_xml_rs::to_string(&input).unwrap();

        self._post("api/user/authentication_login", body)
            .await
            .map_err(|e| HuaweiError {
                code: "authentication_login".to_string(),
                message: e.to_string(),
            })?;

        // Skip checking server proof in scram
        // we assume every thing is okay here
        // Skip saving rsae/rsan since we are not making any requests where those are necessary
        // We can use a monotonic clock since sessions are only kept in memory
        self.logged_in_at = Some(Instant::now());

        Ok(())
    }

    async fn api_challenge(&mut self) -> Result<ChallengeLoginResponse, HuaweiError> {
        let token = self.get_server_token().await.map_err(|e| HuaweiError {
            code: "server_token".to_string(),
            message: e.to_string(),
        })?;

        let (_, request_verification_token) = token.split_at(32);
        self.request_verification_token = request_verification_token.to_string();

        self.client_nonce = Self::generate_nonce();
        let input = ChallengeLoginRequest {
            firstnonce: self.client_nonce.clone(),
            mode: "1".to_string(),
            username: self.username.clone(),
        };
        let body = serde_xml_rs::to_string(&input).unwrap();

        let response = self
            ._post("api/user/challenge_login", body)
            .await
            .map_err(|e| HuaweiError {
                code: "challenge_login".to_string(),
                message: e.to_string(),
            })?;
        self.request_verification_token = response
            .headers()
            .get("__RequestVerificationToken")
            .unwrap()
            .to_str()
            .unwrap()
            .to_string();
        let response = response.text().await.map_err(|e| HuaweiError {
            code: "challenge_response".to_string(),
            message: e.to_string(),
        })?;

        match serde_xml_rs::from_str(&response) {
            Ok(v) => Ok(v),
            Err(_) => {
                let result: HuaweiError = serde_xml_rs::from_str(&response).unwrap();
                Err(result)
            }
        }
    }

    async fn get_server_token(&mut self) -> Result<String, String> {
        let response = self
            ._get("api/webserver/token")
            .await
            .map_err(|e| e.to_string())?;

        let body = response.text().await.map_err(|e| e.to_string())?;
        let response: Element = serde_xml::from_str(&body).map_err(|e| e.to_string())?;
        let token = response.attributes.get("token").unwrap()[0].clone();

        Ok(token)
    }

    // generate_nonce generates 64char nonces from an rng
    fn generate_nonce() -> String {
        let mut buf = [0; 32];
        let mut r = rand::thread_rng();
        r.fill_bytes(&mut buf);

        hex::encode(buf)
    }

    async fn _get(&mut self, path: &str) -> Result<Response, Error> {
        let response = self
            .http_client
            .get(&format!("https://{}/{}", self.address.to_string(), path))
            .send()
            .await?;

        Ok(response)
    }

    async fn _post(
        &mut self,
        path: &str,
        body: impl Into<Body> + ToString,
    ) -> Result<Response, Error> {
        info!(
            "url = {} body = {} token = {}",
            path,
            body.to_string(),
            self.request_verification_token
        );
        let response = self
            .http_client
            .post(&format!("https://{}/{}", self.address.to_string(), path))
            .header(
                "Content-Type",
                "application/x-www-form-urlencoded; charset=UTF-8;",
            )
            .header(
                "__RequestVerificationToken",
                self.request_verification_token.clone(),
            )
            .body(body.into())
            .send()
            .await?;

        Ok(response)
    }

    fn generate_client_proof(&self, resp: ChallengeLoginResponse) -> Result<String, String> {
        let msg = format!(
            "{},{},{}",
            self.client_nonce, resp.servernonce, resp.servernonce
        );
        let mut salted_password = [0; 32];
        pbkdf2_hmac::<Sha256>(
            self.password.as_bytes(),
            &hex::decode(resp.salt).unwrap(),
            resp.iterations,
            &mut salted_password,
        );

        let mut client_key =
            Hmac::<Sha256>::new_from_slice("Client Key".as_bytes()).map_err(|e| e.to_string())?;
        client_key.update(&salted_password);
        let client_key = client_key.finalize().into_bytes();

        let mut stored_key = Sha256::new();
        stored_key.update(client_key);
        let stored_key = stored_key.finalize();

        let mut signature =
            Hmac::<Sha256>::new_from_slice(msg.as_bytes()).map_err(|e| e.to_string())?;
        signature.update(stored_key.as_slice());
        let signature = signature.finalize().into_bytes();

        let mut client_proof = vec![];

        for i in 0..client_key.len() {
            client_proof.push(client_key[i] ^ signature[i])
        }

        Ok(hex::encode(client_proof))
    }
}

#[derive(Serialize, Deserialize)]
#[serde(rename = "request")]
pub struct ListSmsRequest {
    pageindex: u32,
    readcount: u32,
}

#[derive(Deserialize)]
#[serde(rename = "response")]
pub struct ListSmsResponse {
    count: u32,
    messages: Messages,
}

#[derive(Deserialize)]
pub struct Messages {
    message: Vec<Message>,
}

#[derive(Debug, Deserialize)]
#[serde(rename = "message")]
pub struct Message {
    smstat: u64,
    index: u64,
    pub phone: String,
    pub content: String,
    pub date: String,
}

impl Huawei {
    async fn api(
        &mut self,
        path: &str,
        body: impl Into<Body> + ToString,
    ) -> Result<Response, String> {
        self.request_verification_token = self.get_server_token().await?.split_at(32).1.to_string();
        self._post(path, body).await.map_err(|e| e.to_string())
    }

    pub async fn list_sms(&mut self, mut count: u32) -> Result<Vec<Message>, String> {
        let mut msgs = vec![];

        let mut pageindex = 0;
        while count > 0 {
            pageindex += 1;
            let readcount = std::cmp::min(count, 20);

            let req = ListSmsRequest {
                readcount,
                pageindex,
            };
            count -= readcount;

            let body = serde_xml_rs::to_string(&req).unwrap();

            let response = self.api("api/sms/sms-list-contact", body).await?;

            let response = response.text().await.map_err(|e| e.to_string())?;

            let result: ListSmsResponse =
                serde_xml_rs::from_str(&response).map_err(|e| e.to_string())?;

            msgs.extend(result.messages.message);
        }

        Ok(msgs)
    }
}
